package com.yixiang.security.modular.member.service;




import com.baomidou.mybatisplus.extension.service.IService;
import com.yixiang.security.common.persistence.model.StoreMember;
import com.yixiang.security.modular.member.service.dto.MemberDTO;

import java.util.List;

public interface IMemberService extends IService<StoreMember> {
    StoreMember login(String openid);

    List<MemberDTO> memeberList();
}
